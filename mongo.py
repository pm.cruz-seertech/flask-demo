from flask import Flask, jsonify,request, abort, session, redirect, url_for, render_template
from flask_pymongo import PyMongo, ObjectId
from flask_cors import CORS, cross_origin
import json
from bson import ObjectId


app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

app.config['MONGO_DBNAME'] = 'DemoDB'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/DemoDB'

mongo = PyMongo(app)

class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)

@app.route('/')
def index():
    if 'username' in session:
        return 'You are logged in as' + session['username']
    return render_template('index.html')

@app.route('/login', methods=['POST'])
def login():
    users = mongo.db.Users
    data = request.json
    username = data['username']
    password = data['password']

    existinguser = mongo.db.Users.find_one({'username': username,'password': password})

    output = []

    if existinguser is None:
        output.append({'status': 'error', 'message': 'Invalid credentials'})
        return jsonify(output)
    session['username'] = username
    output.append({'status': 'success', 'message': 'User successfully logged-in', 'role': existinguser['role']})
    return jsonify(output)

@app.route('/register', methods=['POST'])
def register():
    users = mongo.db.Users
    data = request.json

    email = data['email']
    username = data['username']
    password = data['password']
    firstname = data['firstname']
    lastname = data['lastname']
    role = data['role']

    existing_user = mongo.db.Users.find_one({'username': username})

    output = []

    if existing_user is None:
        users.insert({"email" : email, "username" : username, "password" : password, "firstname" : firstname, "lastname" : lastname, "role" : "admin"})
        session['username'] = username
        output.append({'status': 'success', 'message': 'Username successfully registered'})
        return jsonify(output)
    output.append({'status': 'error', 'message': 'Username already exists'})
    return jsonify(output)

@app.route('/users', methods=['GET'])
def get_all_users():
    users = mongo.db.Users

    output = []

    for q in users.find():
        output.append({'username' : q['username'],'email' : q['email'],'firstname' : q['firstname'],'lastname' : q['lastname'],'role' : q['role']})
    return jsonify(output)

@app.route('/user/<username>', methods=['GET'])
def get_user(username):
    user = mongo.db.Users.find_one({'username': username})

    output = []
    if user is None:
        output.append(
            {'username': user['username'], 'email': user['email'], 'firstname': user['firstname'],
             'lastname': user['lastname'],
             'role': user['role']})
        return jsonify({'result': output})
    output.append({'status': 'error', 'message': 'Username does not exist'})
    return jsonify({'result': output})

@app.route('/assets', methods=['GET'])
def get_all_assets():
    assets = mongo.db.Assets

    output = []

    for q in assets.find():
        objid = JSONEncoder().encode(q['_id'])
        objid = objid.replace("\"", "")
        output.append({'assetId' : objid , 'assetName' : q['assetName'],'assetType' : q['assetType'],'model' : q['model'],'datePurchased' : q['datePurchased'] ,'status' : q['status']})
    return jsonify(output)

@app.route('/assetrequests', methods=['GET'])
def get_all_asset_requests():
    assetrequests = mongo.db.Asset_Requests
    Users = mongo.db.Users
    Assets = mongo.db.Assets

    output = []

    for q in assetrequests.find():
        userid = q['requestedByUser']
        assetid = q['assetId']
        existinguser = mongo.db.Users.find_one({'_id': ObjectId(userid)})
        existingasset = mongo.db.Assets.find_one({'_id': ObjectId(assetid)})
        if existinguser is not None:
            if existingasset is not None:
                output.append({'assetName' : existingasset['assetName'], 'assetType' : existingasset['assetType'], 'model' : existingasset['model'], 'requestedBy' : existinguser['firstname'] + ' ' + existinguser['lastname'], 'dateRequested' : q['dateRequested'],'status' : q['status']})
            else:
                output.append({'assetName': ' ', 'assetType': ' ',
                               'model': ' ',
                               'requestedBy': existinguser['firstname'] + ' ' + existinguser['lastname'],
                               'dateRequested': q['dateRequested'], 'status': q['status']})
        else:
            if existingasset is not None:
                output.append({'assetName' : existingasset['assetName'], 'assetType' : existingasset['assetType'], 'model' : existingasset['model'], 'requestedBy' : ' ', 'dateRequested' : q['dateRequested'],'status' : q['status']})
            else:
                output.append({'assetName': ' ', 'assetType' : ' ',
                               'model': ' ', 'requestedBy': ' ', 'dateRequested': q['dateRequested'],
                               'status': q['status']})

    return jsonify(output)


@app.route('/assetrequests', methods=['POST'])

def request_asset():
    assetrequests = mongo.db.Asset_Requests

    data = request.json

    assetId = data['assetId']
    requestedByUser = data['requestedByUser']
    dateRequested = data['dateRequested']

    existingassetrequests = mongo.db.Users.find_one({'assetId': assetId, 'requestedByUser': requestedByUser, 'status' : 'Pending'})

    output = []
    if existingassetrequests is None:
        assetrequests.insert({
            "assetId": assetId,
            "requestedByUser": requestedByUser,
            "dateRequested": dateRequested,
            "status": "Pending"}
        )
        output.append({'status': 'success', 'message': 'Asset has been requested'})
        return jsonify({'result': output})
    output.append({'status': 'error', 'message': 'Asset has already been requested'})
    return jsonify({output})


if __name__ == '__main__':
    app.secret_key = 'mysecret'
    app.run(debug=True)